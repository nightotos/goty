import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";

import { NavbarComponent } from "./navbar/navbar.component";
import { GraficobarrahorizontalComponent } from "./graficobarrahorizontal/graficobarrahorizontal.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgxChartsModule } from "@swimlane/ngx-charts";
@NgModule({
  declarations: [NavbarComponent, GraficobarrahorizontalComponent],
  exports: [NavbarComponent, GraficobarrahorizontalComponent],
  imports: [
    CommonModule,
    RouterModule,
    BrowserAnimationsModule,
    NgxChartsModule
  ]
})
export class ComponentsModule {}
