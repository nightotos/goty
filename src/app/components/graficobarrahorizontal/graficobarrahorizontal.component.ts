import { Component, OnInit, OnDestroy, Input } from "@angular/core";

@Component({
  selector: "app-graficobarrahorizontal",
  templateUrl: "./graficobarrahorizontal.component.html",
  styleUrls: ["./graficobarrahorizontal.component.css"]
})
export class GraficobarrahorizontalComponent implements OnDestroy {
  @Input() results: any[] = [];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = true;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = "Juegos";
  showYAxisLabel = true;
  yAxisLabel = "Votos";

  colorScheme = "nightLights";
  intervalo;
  constructor() {
    // this.intervalo = setInterval(() => {
    //   const newResult = [...this.results];
    //   console.log("ticks");
    //   for (let i in newResult) {
    //     newResult[i].value = Math.round(Math.random() * 500);
    //   }
    //   this.results = [...newResult];
    // }, 1500);
  }

  onSelect(event) {
    console.log(event);
  }
  ngOnDestroy() {
    clearInterval(this.intervalo);
  }
}
