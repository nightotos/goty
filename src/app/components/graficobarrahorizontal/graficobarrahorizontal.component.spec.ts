import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficobarrahorizontalComponent } from './graficobarrahorizontal.component';

describe('GraficobarrahorizontalComponent', () => {
  let component: GraficobarrahorizontalComponent;
  let fixture: ComponentFixture<GraficobarrahorizontalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraficobarrahorizontalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficobarrahorizontalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
