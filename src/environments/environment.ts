// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url: "http://localhost:5000/pruebas-softnight/us-central1",
  firebase: {
    apiKey: "AIzaSyDgihhU2Gux1Iy8ITl8G-p7Pf1qD9WS2VQ",
    authDomain: "pruebas-softnight.firebaseapp.com",
    databaseURL: "https://pruebas-softnight.firebaseio.com",
    projectId: "pruebas-softnight",
    storageBucket: "pruebas-softnight.appspot.com",
    messagingSenderId: "314991546704",
    appId: "1:314991546704:web:9f515ffc4bf07f6294dc55",
    measurementId: "G-3546LDK5Y0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
